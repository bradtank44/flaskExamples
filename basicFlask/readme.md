This is a fairly minimal flask app, with two endpoints. One for authors and one for quotes. There is no relation between the two. For each endpoint you can get a list of items, or post a new one. There is some minimal error handling, and no docs.

To run this app, activate your virtualenv with Flask installed
Change directories to basicFlask, and run
python run.py
