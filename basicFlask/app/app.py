from flask import Flask, jsonify, request, abort
app = Flask(__name__)

# Obviously for almost any actual app, storing your data in memory
# is probably a bad idea. So if you're using this as a basis for anything,
# here is where your database set up would likely go

quoteList = ["Do or do not, there is no try",
            "There's a snake in my boot!",
            "I'll be back"]

authorListDict = [{"name": "Stephen King", "genre": "Spook"},
                  {"name": "G.R.R.M", "genre": "Drama, Fantasy"}]

#Here are a pair of curl commands, one to add a quot, , one to get the list
#curl localhost:5000/api/quotes/ -X post -d '{"quote":"Feeling lucky, punk?"}' -H "Content-Type: application/json"
#curl localhost:5000/api/quotes/

@app.route('/api/quotes/', methods=['GET', 'POST'])
def quotes():
    if request.method == 'POST':
        content = request.get_json()
        return addQuote(content['quote'])
    else:
        return jsonify(quoteList)

def addQuote(quote):
    if quote == "":
        return '{"status":"fail", "message":"The quote can\'t be null or empty"}', 400

    quoteList.append(quote)
    return '{"status":"success"}', 201

#curl localhost:5000/api/authors
#curl localhost:5000/api/authors -X post -d '{"author":"J R.R. Tolkien", "genre":"Fantasy"}' -H "Content-Type: application/json"
@app.route('/api/authors/', methods=['GET', 'POST'])
def authors():
    if request.method == 'POST':
        content = request.get_json()
        return addAuthor(content['author'], content['genre'])
    else:
        return jsonify(authorListDict)

def addAuthor(name, genre):
    if name == "":
        return '{"status":"fail", "message":"The name can\'t be null or empty"}', 400
    if genre == "":
        return '{"status":"fail", "message":"The genre can\'t be null or empty"}', 400

    authorListDict.append({"name": name, "genre": genre})
    return '{"status":"success"}', 201



#This way we can just run the app from the command line, so rather than
# `set FLASK_APP=app.py`
# `flask run`
# We can just run it like any other file
# `./app.py`
def main():
    app.run(debug=True, host='0.0.0.0')

if __name__ == "__main__":
    main()
