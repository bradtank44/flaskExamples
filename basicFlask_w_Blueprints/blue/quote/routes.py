from flask import Blueprint, Flask, jsonify, request, abort

mod = Blueprint ('quote', __name__)

quoteList = ["Do or do not, there is no try",
            "There's a snake in my boot!",
            "I'll be back"]

#Here are a pair of curl commands, one to add a quot, , one to get the list
#curl localhost:5000/api/quotes/ -X post -d '{"quote":"Feeling lucky, punk?"}' -H "Content-Type: application/json"
#curl localhost:5000/api/quotes/

@mod.route('/', methods=['GET', 'POST'])
def quotes():
    if request.method == 'POST':
        content = request.get_json()
        return addQuote(content['quote'])
    else:
        return jsonify(quoteList)

def addQuote(quote):
    if quote == "":
        return '{"status":"fail", "message":"The quote can\'t be null or empty"}', 400

    quoteList.append(quote)
    return '{"status":"success"}', 201
