#This one actuually imports everything

from flask import Flask

app = Flask(__name__)

from blue.author.routes import mod
from blue.quote.routes import mod

app.register_blueprint(author.routes.mod, url_prefix='/api/authors')
app.register_blueprint(quote.routes.mod, url_prefix='/api/quotes')
