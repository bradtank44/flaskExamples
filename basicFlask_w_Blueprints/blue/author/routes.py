from flask import Blueprint, Flask, jsonify, request, abort

mod = Blueprint ('author', __name__)

authorListDict = [{"name": "Stephen King", "genre": "Spook"},
                  {"name": "G.R.R.M", "genre": "Drama, Fantasy"}]


#curl localhost:5000/api/authors/
#curl localhost:5000/api/authors/ -X post -d '{"author":"J R.R. Tolkien", "genre":"Fantasy"}' -H "Content-Type: application/json"
@mod.route('/', methods=['GET', 'POST'])
def authors():
    if request.method == 'POST':
        content = request.get_json()
        return addAuthor(content['author'], content['genre'])
    else:
        return jsonify(authorListDict)

def addAuthor(name, genre):
    if name == "":
        return '{"status":"fail", "message":"The name can\'t be null or empty"}', 400
    if genre == "":
        return '{"status":"fail", "message":"The genre can\'t be null or empty"}', 400

    authorListDict.append({"name": name, "genre": genre})
    return '{"status":"success"}', 201
